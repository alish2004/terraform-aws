#-------------------------------------------------------------------------
# Provision Hihly Available Web server in any AWS Region with default VPC
# Create:
#  - 1. Security Group for the Web server
#  - 2. Launch Configuration with Auto AMI Lookup
#  - 3. Auto Scaling Group using 2 AWS Availability Zones
#  - 4. Classic Elastic Load Balancer in 2 AWS Availability Zones
#
# Made by Alisher Yuldashev 12/2021
#----------------------------------------------------------------------------

provider "aws" {
  region = "us-east-1"
}

data "aws_availability_zones" "available" {}
#  AWS Linux-2 AMI
data "aws_ami" "latest_amazon_linux" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
#-------------------------------------------------------------
# 1. Configuring AWS Security Group

resource "aws_security_group" "web" {
  name        = "AWS Security Group"
  description = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["80", "443"]

    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.10.10.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Dynamic SG"
    Owner = "Alisher Yuldashev"
  }

}

# 2. Launch Configuration with Auto AMI Lookup
resource "aws_launch_configuration" "web" {
  //name           = "WebServer-Higly-Available-LC"
  name_prefix = "WebServer-Higly-Available-LC-"

  image_id        = data.aws_ami.latest_amazon_linux.id
  instance_type   = "t3.micro"
  security_groups = [aws_security_group.web.id]
  user_data       = file("user_data.sh")

  lifecycle {
    create_before_destroy = true
  }
}

### Auto Scaling Group using 2 AWS Availability Zones

resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  health_check_type    = "ELB"
  vpc_zone_identifier  = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  load_balancers       = [aws_elb.web.name]

  dynamic "tag" {
    for_each = {
      Name   = "WebServer in ASG"
      Owner  = " Alisher Yuldashev"
      TAGKEY = " TAGVALUE"
    }
    content {
      key                 = "tag.key"
      value               = "tag.value"
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}
#------------------------------------------------------------------------
# 4. Classic Elastic Load Balancer in 2 AWS Availability Zones
resource "aws_elb" "web" {
  name               = "WebServer-HA-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10

  }

  tags = {
    Name = "WebServer-Higly-Available-ELB"
  }
}

resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

# Outputs ----------------------------------------------------------------------
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}
