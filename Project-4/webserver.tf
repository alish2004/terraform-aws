#------------------------------------------------------------------------------
# AWS EC2 Terraform script will
#
# Build Web Server + static public IP during Bootstrap and apply template file
# Resources Lifecycle and Zero Down Time during changes TF resources in this code
#
# Made by Alisher Yuldashev 12/2021
#-------------------------------------------------------------------------------

provider "aws" {
  region = "us-east-1"
}

#This command below will create a new Elastic static public IP and will assign it to our new AWS EC2
resource "aws_eip" "new_static_ip" {
  instance = aws_instance.webserver.id
}

# Resources are the most important element in the Terraform language.
# Each resource block describes one or more infrastructure objects, such as virtual networks,
# compute instances, or higher-level components such as DNS records.

resource "aws_instance" "webserver" {

# If we need to create 10 exactly the same AWS EC2 instances, then we use count 10
  #count = 10

# This image below from AWS MarketPlace Amazon Linux AMI:
  ami = "ami-0ed9277fb7eb570c9"
# You need to specify type of the instance
  instance_type = "t2.micro"
# This is the current subnet ID
  subnet_id     = "subnet-9ok96re"
  # This is new security group
 vpc_security_group_ids = [aws_security_group.webserver_sg.id]

# This is our script file and data
 user_data   = templatefile("script.tpl", {
   f_name    = "Alisher",
   l_name    = "Yuldashev",
   names     = ["Mike", "Nick", "John", "Ronald", "Mariya", "Alexa", "Peter", "Jack"]
   })

   # If we need to prevent removing certain parameters we can use prevent_destroy and ignore command
   ##lifecycle {
   ##  prevent_destroy =true
    ##  ignore_changes = [ami, user_data]
    ##  }

# If we did some chnages on this TF file, then this command below will apply new changes first, it means recreate new EC2 server with new parameters, then it will #destroy the old EC2.  
      lifecycle {
      create_before_destroy = true
      }

 # This is my AWS SSH kepair name, yes it works for Windows as well
 key_name  = "my_ssh_key"
# This place is for Tags
 tags = {
 Name    = "Linux-Web-server"
 Owner   = "Alisher Yuldashev"
 Project = "Terraform"
 }
}

resource "aws_security_group" "webserver_sg" {
  name        = "Webserver Security Group"
  description = "Allow inbound traffic"


# We are opening ports
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["10.10.10.55/32"]
    }
  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }
  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

# If you select a protocol of "-1" (semantically equivalent to all ports) in our case below it means open to all outside ports.
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
    Name = "Webserver SG"
    }

  }
