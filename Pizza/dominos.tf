# Using this terraform script below, you can order Dominos Pizza
# You would need to download binary file https://github.com/ndmckinley/terraform-provider-dominos/tree/master/bin
# Made by Alisher Yuldashev
#---------------------------------------------------------------------


terraform {
  required_providers {
    dominos = {
      source  = "dominos.com/myorg/dominos" # home directory /.terraform.d/plugins/dominos.com/myorg/dominos/1.0/linux_amd64/
      version = "~>1.0"
    }
  }
}


# Credit card owner info
provider "dominos" {
  first_name    = "Alisher"
  last_name     = "Yuldashev"
  email_address = "alisher@gmail.com"
  phone_number  = "1-345-2454565"


  #Credit card info
  credit_card {
    number = 1234123412341234
    cvv    = 555
    date   = "06/2025"
    zip    = 11220
  }
}


data "dominos_address" "myaddress" {
  street = "917 Coney Island Ave"
  city   = "Brooklyn"
  state  = "NY"
  zip    = "12230"
}
#---------------------------------------------
output "dominons_address" {
  value = data.dominos_address.myaddress.url_object
}


data "dominos_store" "closest" {
  address_url_object = data.dominos_address.myaddress.url_object
}

data "dominos_menu_item" "item" {
  store_id     = data.dominos_store.closest.store_id
  query_string = ["coke"]
}

resource "dominos_order" "myorder" {
  address_api_object = data.dominos_address.myaddress.api_object
  store_id           = data.dominos_store.closest.store_id
  item_codes         = ["P14IBKCZ", "20BCOKE"]
}

# P14IBKCZ - Brooklyn Large Pizza
# 20BCOKE  - 20 Oz Cooke

output "dominos_store" {
  value = data.dominos_store.closest.store_id
}

# output "dominos_menu" {
#  value = data.dominos_menu_item.item
#  }

output "dominos_order" {
  value = dominos_order.myorder
}
