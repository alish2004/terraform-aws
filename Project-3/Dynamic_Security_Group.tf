#--------------------------------------------
# AWS EC2 Terraform script
#
# Build Dynamic_Security_Group during Bootstrap
#
# Made by Alisher Yuldashev 12/2021
#---------------------------------------------

provider "aws" {
  region = "us-east-1"
}

# Configuring AWS Security Group

resource "aws_security_group" "AWS_SG" {
  name        = "AWS Security Group"
  description = "Dynamic Security Group"

# ingress means open ports on this AWS server or service

dynamic "ingress" {
  for_each = ["161", "443", "80", "8080", "3306"]

content {
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["10.10.0.0/16"]
    }
    
# If you select a protocol of "-1" (semantically equivalent to all ports) in our case below it means open to all ports.
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
    Name = "Dynamic SG"
    }

}
