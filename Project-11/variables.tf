variable "region" {
  description = "Please Enter AWS Region to deploy Web Server"
  type        = string
  default     = "us-east-1"
}

variable "instance_type" {
  description = "Enter Instance Type"
  type        = string
  default     = "t3.small"
}

variable "allow_ports" {
  description = "List of open ports for server"
  type        = list(any)
  default     = ["22", "80", "443", "8080", "3306"]
}

variable "enable_detail_monitoring" {
  type    = bool
  default = "false"
}

variable "common_tags" {
  description = "Common Tags to Apply on All Resources"
  type        = map(any)
  default = {
    Owner       = "Alisher Yuldashev"
    Project     = "Terraform-Web"
    CostCenter  = "607080"
    Environment = "Development"
  }
}
