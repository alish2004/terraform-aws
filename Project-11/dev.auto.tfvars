# Auto Fill parameters fro DEV
# File can be named as
# terraform.tfvars  or any filename .tfvars
# for example dev.tfvars or stg.tfvars

region                     = "us-east-2"
instance_type              = "t2.micro"
enabable_detail_monitoring = false

allow_ports                =  ["22", "80", "443", "8443", "3306"]

common_tags = {
  Owner       = "Alisher"
  Project     = "Terraform-DEV"
  CostCenter  = "102030"
  Environment = "Dev"
}
