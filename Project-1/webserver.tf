#--------------------------------------------------------------------
# AWS EC2 Terraform script
#
# Build Web Server, SG,  during Bootstrap using static script files.
# in my case  user_data = file
#
# Made by Alisher Yuldashev
#---------------------------------------------------------------------

# This Terraform code will be install new AWS EC2 Apache Web server using a bash script file sript.sh


provider "aws" {
  region = "us-east-1"
}
# Resources are the most important element in the Terraform language.
#Each resource block describes one or more infrastructure objects, such as virtual networks,
# compute instances, or higher-level components such as DNS records.

resource "aws_instance" "webserver" {
# If we need to create 10 exactly the same AWS EC2 instances, then we use count 10
  count = 1
# This image below from AWS MarketPlace Amazon Linux AMI:
  ami = "ami-0ed9277fb7eb570c9"

# You need to specify type of the AWS instance
  instance_type = "t2.micro"

# This is the current AWS subnet ID
  subnet_id = "subnet-7b4k90gt"

# This is new security group
  vpc_security_group_ids = [aws_security_group.webserver_sg.id]

  # This is my AWS SSH kepair name, yes it works for Windows as well
   key_name  = "my_ssh_key"

   # Here I specify which script file will be applied during a bootstrap
   user_data = file("script.sh")

# This is place for Tags
 tags = {
 Name = "Linux-Web-server"
 Owner = "Alisher Yuldashev"
 Project = "Terraform"
 }
}

resource "aws_security_group" "webserver_sg" {
  name        = "Webserver Security Group"
  description = "Allow inbound traffic"

# We are opening ports
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["10.10.10.55/32"]
    }
  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }
  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
    Name = "Webserver SG"

  }
}
