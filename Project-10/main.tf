#--------------------------------------------
# AWS EC2 Terraform script
#
# Build AWS Ec2 Server using TF TAG variables
#
# Made by Alisher Yuldashev 12/2021
#---------------------------------------------

provider "aws" {
  region = var.region
}

data "aws_ami" "latest_amazon_linux" {
  owners      = ["137112412989"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_eip" "new_elastic_ip" {
  instance = aws_instance.webserver.id
  //tags     = var.common_tags
  tags     = merge(var.common_tags,{ Name = "${var.common_tags["Environment"]} Server IP"})
}

resource "aws_instance" "webserver" {
  ami                    = data.aws_ami.latest_amazon_linux.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.webserver.id]
  monitoring             = var.enable_detail_monitoring
  tags     = merge(var.common_tags,{ Name = "${var.common_tags["Environment"]} Server build by Terraform"})
}

  resource "aws_security_group" "webserver" {
    name = "My SG"
  dynamic "ingress" {
    for_each = var.allow_ports
    content {
      from_port = ingress.value
      to_port = ingress.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

  }

egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

   tags = merge(var.common_tags,{ Name = "${var.common_tags["Environment"]} Server Security Group"})

  }
