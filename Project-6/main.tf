#--------------------------------------------
# AWS EC2 Terraform script
#
# Build Web Server during Bootstrap
#
# Made by Alisher Yuldashev 12/2021
#---------------------------------------------

provider "aws" {
  region = "us-east-1"
}

# Resources are the most important element in the Terraform language.
# Each resource block describes one or more infrastructure objects, such as virtual networks,
# compute instances, or higher-level components such as DNS records.

resource "aws_instance" "WEB-Server" {

# If we need to create 10 exactly the same AWS EC2 instances, then we use count 10
  #count = 10

# This image below from AWS MarketPlace Amazon Linux AMI:
  ami                   = "ami-0ed9277fb7eb570c9"
# You need to specify type of the instance
  instance_type         = "t2.micro"
# This is the current subnet ID
  subnet_id             = "subnet-9b2k83kl"
# This is new security group
 vpc_security_group_ids = [aws_security_group.webserver_sg.id]

 tags = {
   Name = "WEB-Server"
  }
  # This is my AWS SSH kepair name, yes it works for Windows as well
  key_name  = "my_ssh_key"

  # WEB server won't be created untill DB and APP servers will be created first. So that's why we use depends command
  depends_on = [aws_instance.DB-Server, aws_instance.WEB-Server]
 }

resource "aws_instance" "APP-Server" {
  ami                    = "ami-0ed9277fb7eb570c9"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-9b2k83kl"
  vpc_security_group_ids = [aws_security_group.webserver_sg.id]

 tags = {
   Name = "APP-Server"
  }
  # This is my AWS SSH kepair name, yes it works for Windows as well
  key_name  = "terraform"

  # APP server won't be created untill DB server will be created first. So that's why we use depends command
  depends_on = [aws_instance.DB-Server]
 }

 resource "aws_instance" "DB-Server" {
   ami                    = "ami-0ed9277fb7eb570c9"
   instance_type          = "t2.micro"
   subnet_id              = "subnet-9b2k83kl"
   vpc_security_group_ids = [aws_security_group.webserver_sg.id]

    tags = {
      Name = "DB-Server "
}
 # This is my AWS SSH kepair name, yes it works for Windows as well
 key_name  = "terraform"

}

 # We define rules for the AWS Security Group
resource "aws_security_group" "webserver_sg" {
  name        = "Webserver SG"
  description = "Allow inbound traffic"

# We are opening ports
dynamic "ingress" {
for_each = ["80", "443", "22"]
content {
from_port = ingress.value
  to_port = ingress.value
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
   }
 }

    tags = {
    Name = "Webserver SG"
    }
  }
