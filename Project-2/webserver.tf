#--------------------------------------------
# AWS EC2 Terraform script
#
# Build Web Server during Bootstrap using dynamic templates files 
# In my case I used: user_data = templatefile
#
# Made by Alisher Yuldashev 12/2021
#---------------------------------------------

# This terraform script will create new AWS EC-2 server + install Security Group, Apache Web server and insert it to the user-data template.

provider "aws" {
  region = "us-east-1"
}
# Resources are the most important element in the Terraform language.
# Each resource block describes one or more infrastructure objects, such as virtual networks,
# compute instances, or higher-level components such as DNS records.

resource "aws_instance" "webserver" {
# If we need to create 10 exactly the same AWS EC2 instances, then we use count command.
  count = 1
# This image below from AWS MarketPlace Amazon Linux AMI:
  ami = "ami-0ed9277fb7eb570c9"
# You need to specify type of the instance
  instance_type = "t2.micro"
# This is the current subnet ID
  subnet_id = "subnet-9b2c67aj"
# This is new security group
 vpc_security_group_ids = [aws_security_group.webserver_sg.id]
 
 ## This template script script.tpl will install apache web server and insert data.
 user_data = templatefile("script.tpl", {
   f_name = "Alisher",
   l_name = "Yuldashev",
   names  = ["Mike", "Nick", "John", "Ronald", "Mariya", "Alexa"]
   })
     # This is my AWS SSH kepair name, yes it works for Windows as well
      key_name  = "my_ssh_key"
# This is place for Tags
 tags = {
 Name = "Linux-Web-server"
 Owner = "Alisher Yuldashev"
 Project = "Terraform"
 }
}

resource "aws_security_group" "webserver_sg" {
  name        = "Webserver Security Group"
  description = "Allow inbound traffic"

# We are opening ports
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["10.10.10.55/32"]
    }
  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }
  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    }

# If you select a protocol of "-1" (semantically equivalent to all ports) in our case below it means open to all ports.

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
    Name = "Webserver SG"
    }

}
