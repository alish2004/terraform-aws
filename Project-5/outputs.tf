# we can see our output from this terraform script.
output "webserver_instance_id" {
  value = aws_instance.webserver.id
}

output "webserver_public_ip_address" {
  value = aws_eip.new_static_ip.public_ip
}

output "webserver_sg_id" {
  value =  aws_security_group.webserver_sg.id
}

output "webserver_sg_arn" {
  value = aws_security_group.webserver_sg.arn
  description = "You can see this Security Group ARN in your output"
}
